# weather-tgbot
Open source weather bot for telegram. Made by kaikkitietokoneista.net/bots.

## Demo

I have a working bot in telegram with this code. You can go to chat with it in an url http://t.me/ktweather_bot.

## Installation

You must install Node.JS at first.


```console
apt install nodejs 
apt install npm

```

Install dependencies.


```console
npm install openweather-apis
npm install node-telegram-bot-api

```

Then you need to copy app.js file and run it with command.

```console
node app.js

```

## To do

1. Fix code to use || when or
